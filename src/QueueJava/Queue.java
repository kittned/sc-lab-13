package QueueJava;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Queue {
	private ArrayList<String> data;
	private Lock dataLock;
	private Condition condition;
	
	public Queue(){
		data = new ArrayList<String>();
		dataLock = new ReentrantLock();
		condition = dataLock.newCondition();
	}
	public void enqueue(String str) throws InterruptedException{
		dataLock.lock();
		try{
			while(data.size()>=10)
				condition.await();
				
			data.add(str);
			condition.signalAll();
		}
		finally{
			dataLock.unlock();
		}
	}
	public void dequeue() throws InterruptedException{
		dataLock.lock();
		try{
			while(data.size()==0)
				condition.await();
			System.out.println(data.get(0));
			data.remove(0);
			condition.signalAll();
		}
		finally{
			dataLock.unlock();
		}
	}
}
	
