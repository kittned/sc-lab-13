package QueueJava;

public class ProducerConsumerMain {
	public static void main (String[] args){
		Queue q = new Queue();
		
		Producer p = new Producer(q);
		Consumer c = new Consumer(q);
		
		Thread t = new Thread(p);
		Thread t2 = new Thread(c);
		
		t.start();
		t2.start();
	}
}
