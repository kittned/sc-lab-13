package QueueJava;

import java.util.Date;

public class Producer implements Runnable{
	private static final int DELAY = 2000;
	private Queue q;
	
	public Producer(Queue q){
		this.q=q;
	}
	public void run(){
		try {
			for(int i=1 ; i<=100; i++){
				q.enqueue(new Date().toString());
				Thread.sleep(DELAY);
			}
		}
		catch (InterruptedException exception){}
	}

}
