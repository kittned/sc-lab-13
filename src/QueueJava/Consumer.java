package QueueJava;

public class Consumer implements Runnable{
	private static final int DELAY = 1000;
	private Queue q;
	public Consumer(Queue q){
		this.q = q;
	}
	public void run(){
		try {
			for(int i=1 ; i<=100; i++){
				q.dequeue();
				Thread.sleep(DELAY);
			}
		}
		catch (InterruptedException exception){}
	}

	
}
